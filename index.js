require('dotenv').config()

const express = require('express')
const mongoose = require('mongoose')
const session = require('express-session')
const cookieParser = require('cookie-parser')
const MongoStore = require('connect-mongo')(session)
const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy

const isProduction = process.env.NODE_ENV === 'production'

// =================
// AUTH KIT STRATEGY
// =================

function createLocalStrategy (userModel) {
  return new LocalStrategy(async (username, password, done) => {
    const user = await userModel
      .findOne({ username })
      .select('+password +roles')

    if (!user) {
      return done(null, false, { message: 'Username salah' })
    }

    const correct = await user.checkPassword(password)

    if (!correct) {
      return done(null, false, { message: 'Password salah' })
    }

    done(null, user)
  })
}

function createSerializer () {
  return (user, done) => done(null, user._id)
}

function createDeserializer (userModel) {
  return (id, done) => userModel
    .findById(id)
    .then(user => done(null, user))
    .catch(err => done(err))
}

// ===========================
// FUNCTION TO CREATE FULL APP
// ===========================

async function create (config) {
  const app = express()

  // ==============
  // REQUEST PARSER
  // ==============

  console.log('[✓] Setup request parsers...')

  app.use(express.json())
  app.use(express.urlencoded({ extended: true }))

  app.use(cookieParser(config.secret))

  app.use(session({
    secret: config.secret,
    store: new MongoStore({ mongooseConnection: mongoose.connection }),
    cookie: { maxAge: 1000 * 60 * 60 },
    resave: false,
    saveUninitialized: true
  }))

  // ===================
  // DATABASE AND MODELS
  // ===================

  console.log('[✓] Setup database and models...')

  await mongoose.connect(config.databaseURL, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false
  })

  const Config = require('./models/config').Model
  const User = require('./models/user').Model
  const Info = require('./models/info').Model
  // const Practicum = require('./models/practicum').Model

  // ==============
  // AUTHENTICATION
  // ==============

  console.log('[✓] Setup authentication...')

  passport.use(createLocalStrategy(User))
  passport.serializeUser(createSerializer())
  passport.deserializeUser(createDeserializer(User))

  app.use(passport.initialize())
  app.use(passport.session())

  // ======
  // ROUTES
  // ======

  console.log('[✓] Setup routes...')

  app.use(require('./routes/auth')({ User }))
  app.use('/access-control', require('./routes/access-control')({ User, Config }))
  app.use('/user', require('./routes/user')({ User }))
  app.use('/info', require('./routes/info')({ Info }))

  return app
}

// =========================
// SHOW ENVIRONMENT VARIABLE
// =========================

const varRequired = [
  'MONGODB_URL',
  'APP_SECRET',
  'APP_PORT'
]

console.log(`[✓] MODE = ${isProduction ? 'PRODUCTION' : 'DEVELOPMENT'}`)

varRequired.map(l => console.log(
  `[${process.env[l] ? '✓' : 'x'}] ENV:${l} = ${process.env[l]}`
))

// ==================
// CREATE APP AND RUN
// ==================

create({
  databaseURL: process.env.MONGODB_URL,
  secret: process.env.APP_SECRET
}).then(app => {
  console.log('[✓] App listening...')
  app.listen(process.env.APP_PORT)
}).catch(err => console.error(err))
