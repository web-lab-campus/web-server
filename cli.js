require('dotenv').config()

const program = require('commander')
const mongoose = require('mongoose')

async function createDBConnection () {
  await mongoose.connect(process.env.MONGODB_URL, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false
  })
}

function closeDBConnection () {
  mongoose.connection.close()
}

program
  .command('admin:add <username>')
  .description('Add an user administrator')
  .action(async function (username) {
    await createDBConnection()
    const User = require('./models/user').Model

    const user = await User
      .findOne({ username })
      .select('+roles')

    if (!user) {
      return console.log(`User "${username}" not found`)
    }

    user.roles.push('admin')
    await user.save()

    console.log('Done!')
    closeDBConnection()
  })

program
  .command('admin:remove <username>')
  .description('Add an user administrator')
  .action(async function (username) {
    await createDBConnection()
    const User = require('./models/user').Model

    const user = await User
      .findOne({ username })
      .select('+roles')

    if (!user) {
      return console.log(`User "${username}" not found`)
    }

    user.roles.pull('admin')
    await user.save()

    console.log('Done!')
    closeDBConnection()
  })

program
  .command('admin:list')
  .description('Add an user administrator')
  .action(async function () {
    await createDBConnection()
    const User = require('./models/user').Model

    const user = await User
      .find({ roles: 'admin' })
      .select('+roles')

    console.table(user.map(l => ({
      ID: l._id,
      Name: l.name,
      Username: l.username
    })))

    closeDBConnection()
  })

program.parse(process.argv)
