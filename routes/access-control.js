const Router = require('express').Router
const wrap = require('./_util').wrap
const isLogin = require('./_util').isLogin

const isAdmin = (req, res, next) => req.user.roles.find(r => r === 'admin')
  ? next()
  : res.status(401).send('Unauthorized')

module.exports = function ({ User, Config }) {
  const router = new Router()

  const editableRoles = [
    'manager:user',
    'manager:info'
  ]

  // ==========
  // WEB CONFIG
  // ==========

  router.get('/web-config', wrap(async (req, res) => {
    const config = await Config
      .find()
      .where('key')
      .in(req.query.fields)

    const tableConfig = {}
    config.map(l => {
      tableConfig[l.key] = l.value
    })

    res
      .status(200)
      .json({ config: tableConfig })
  }))

  // =================
  // UPDATE WEB CONFIG
  // =================

  router.post('/update-web-config', isLogin, isAdmin, wrap(async (req, res) => {
    let toChange

    for (let i in req.body.fields) {
      toChange = await Config.findOne({ key: i })

      if (toChange) {
        toChange.value = req.body.fields[i]
      } else {
        toChange = new Config({
          key: i,
          value: req.body.fields[i]
        })
      }

      await toChange.save()
    }

    const config = await Config
      .find()
      .where('key')
      .in(Object.keys(req.body.fields))

    const tableConfig = {}
    config.map(l => {
      tableConfig[l.key] = l.value
    })

    res
      .status(200)
      .json({ config: tableConfig })
  }))

  // =======
  // GET ALL
  // =======

  router.get('/all', isLogin, isAdmin, wrap(async (req, res) => {
    const manager = await Promise.all([
      User.find({ roles: 'admin' }),
      User.find({ roles: 'manager:user' }),
      User.find({ roles: 'manager:info' })
    ])

    const admin = manager[0]
    const managerUser = manager[1]
    const managerInfo = manager[2]

    res
      .status(200)
      .json({
        admin,
        manager_user: managerUser,
        manager_info: managerInfo
      })
  }))

  // ==========
  // ADD ACCESS
  // ==========

  router.post('/add', isLogin, isAdmin, wrap(async (req, res, next) => {
    const errors = {}

    if (!req.body.username) {
      errors.username = 'Username tidak boleh kosong'
    }

    if (!req.body.role) {
      errors.role = 'Pilih peran pengguna'
    } else if (!editableRoles.find(r => r === req.body.role)) {
      errors.role = 'Sepertinya peran yang dipilih tidak bisa diterapkan'
    }

    if (Object.keys(errors).length) {
      return res
        .status(422)
        .json({ errors })
    }

    const user = await User.findOne({ username: req.body.username })

    if (!user) {
      return res
        .status(422)
        .json({ errors: {
          username: 'Username ini tidak ditemukan'
        } })
    }

    if (user.roles.find(r => r === req.body.role)) {
      return res
        .status(422)
        .json({ errors: {
          username: 'Username ini sudah memiliki peran dimaksud'
        } })
    }

    user.roles.push(req.body.role)
    await user.save()

    const manager = await Promise.all([
      User.find({ roles: 'manager:user' }),
      User.find({ roles: 'manager:info' })
    ])

    const managerUser = manager[0]
    const managerInfo = manager[1]

    res
      .status(200)
      .json({
        manager_user: managerUser,
        manager_info: managerInfo
      })
  }))

  // =============
  // REMOVE ACCESS
  // =============

  router.post('/remove', isLogin, isAdmin, wrap(async (req, res, next) => {
    const errors = {}

    if (!req.body.username) {
      errors.username = 'Username tidak boleh kosong'
    }

    if (!req.body.role) {
      errors.role = 'Pilih peran pengguna'
    } else if (!editableRoles.find(r => r === req.body.role)) {
      errors.role = 'Sepertinya peran yang dipilih tidak bisa diterapkan'
    }

    if (Object.keys(errors).length) {
      return res
        .status(422)
        .json({ errors })
    }

    const user = await User.findOne({ username: req.body.username })

    if (!user) {
      return res
        .status(422)
        .json({ errors: {
          username: 'Username ini tidak ditemukan'
        } })
    }

    if (!user.roles.find(r => r === req.body.role)) {
      return res
        .status(422)
        .json({ errors: {
          username: 'Username ini tidak memiliki peran dimaksud'
        } })
    }

    user.roles.pull(req.body.role)
    await user.save()

    const manager = await Promise.all([
      User.find({ roles: 'manager:user' }),
      User.find({ roles: 'manager:info' })
    ])

    const managerUser = manager[0]
    const managerInfo = manager[1]

    res
      .status(200)
      .json({
        manager_user: managerUser,
        manager_info: managerInfo
      })
  }))

  return router
}
