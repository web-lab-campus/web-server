const Router = require('express').Router
const bcrypt = require('bcrypt')
const wrap = require('./_util').wrap
const isLogin = require('./_util').isLogin

module.exports = function ({ User }) {
  const router = new Router()

  const allowed = {
    toUpdateUser: (userInDatabase, user) => {
      if (userInDatabase._id.equals(user._id)) return true
      if (user.roles && user.roles.find(r => r === 'manager:user')) return true
      if (user.roles && user.roles.find(r => r === 'admin')) return true
      return false
    },

    toBypassPassword: (userInDatabase, user) => {
      if (user.roles && user.roles.find(r => r === 'manager:user')) return true
      if (user.roles && user.roles.find(r => r === 'admin')) return true
      return false
    }
  }

  // =========
  // GET BY ID
  // =========

  router.get('/:id', wrap(async (req, res) => {
    const user = await User.findById(req.params.id)

    if (!user) {
      return res
        .status(404)
        .send('User is not found')
    }

    const editable = req.user && allowed.toUpdateUser(user, req.user)

    res
      .status(200)
      .json({ user, editable })
  }))

  // ===========
  // PATCH BY ID
  // ===========

  router.patch('/:id', isLogin, wrap(async (req, res) => {
    let user = await User
      .findById(req.params.id)
      .select('+password')

    if (!user) {
      return res
        .status(404)
        .send('User is not found')
    }

    if (!allowed.toUpdateUser(user, req.user)) {
      return res
        .status(401)
        .send('Unauthorized')
    }

    if (req.body.password && !allowed.toBypassPassword(user, req.user)) {
      const matched = await bcrypt.compare(
        req.body.old_password,
        user.password
      )

      if (!matched) {
        return res
          .status(422)
          .json({ errors: { old_password: 'Password lama salah' } })
      }
    }

    user.set(req.body)

    await user.save({ validateBeforeSave: true })

    user = await User.findById(req.params.id)

    res
      .status(200)
      .json({ user })
  }))

  return router
}
