const Router = require('express').Router
const wrap = require('./_util').wrap
const isLogin = require('./_util').isLogin
const ObjectId = require('mongoose').Types.ObjectId

module.exports = function ({ Info }) {
  const router = new Router()

  const allowed = {
    toEditInfo: (info, user) => {
      if (info.author && info.author._id.equals(user._id)) return true
      if (user.roles && user.roles.find(r => r === 'manager:info')) return true
      if (user.roles && user.roles.find(r => r === 'admin')) return true
      return false
    },

    toCreateInfo: (user) => {
      if (user.roles && user.roles.find(r => r === 'manager:info')) return true
      if (user.roles && user.roles.find(r => r === 'admin')) return true
      return false
    }
  }

  // ========
  // LIST ALL
  // ========

  router.get('/', wrap(async (req, res) => {
    let cursor = Info.find().sort({ _id: -1 })

    if (req.query.paginate && Number(req.query.paginate) > 0) {
      cursor = cursor.limit(Number(req.query.paginate))
    }

    if (req.query.before && ObjectId.isValid(req.query.before)) {
      cursor = cursor.lt('_id', req.query.before)
    }

    if (req.query.search && String(req.query.search).length) {
      cursor = cursor.where({ $text: { $search: String(req.query.search) } })
    }

    let info = await cursor

    info = info.map(l => {
      const spliten = l.content.split(`\n`)
      l.content = spliten[0]
      let i = 1

      while (l.content.length < 300 && typeof spliten[i] !== 'undefined') {
        l.content += `\n` + spliten[i]
        i++
      }

      return l
    })

    const creatable = req.user && allowed.toCreateInfo(req.user)

    res
      .status(200)
      .json({ info, creatable })
  }))

  // ======
  // CREATE
  // ======

  router.post('/', isLogin, wrap(async (req, res) => {
    if (!allowed.toCreateInfo(req.user)) {
      return res
        .status(401)
        .send('Unauthorized')
    }

    const info = new Info({
      title: req.body.title,
      content: req.body.content,
      author: req.user._id
    })

    await info.save()

    res
      .status(200)
      .json({ info })
  }))

  // =========
  // GET BY ID
  // =========

  router.get('/:id', wrap(async (req, res) => {
    const info = await Info.findById(req.params.id)

    if (!info) {
      return res
        .status(404)
        .send('Information is not found')
    }

    const editable = req.user && allowed.toEditInfo(info, req.user)

    res
      .status(200)
      .json({ info, editable })
  }))

  // ===========
  // PATCH BY ID
  // ===========

  router.patch('/:id', isLogin, wrap(async (req, res) => {
    const info = await Info.findById(req.params.id)

    if (!info) {
      return res
        .status(404)
        .send('Information is not found')
    }

    if (!allowed.toEditInfo(info, req.user)) {
      return res
        .status(401)
        .send('Unauthorized')
    }

    info.set(req.body)

    await info.save()

    res
      .status(200)
      .json({ info })
  }))

  // ============
  // DELETE BY ID
  // ============

  router.delete('/:id', isLogin, wrap(async (req, res) => {
    const info = await Info.findById(req.params.id)

    if (!info) {
      return res
        .status(404)
        .send('Information is not found')
    }

    if (!allowed.toEditInfo(info, req.user)) {
      return res
        .status(401)
        .send('Unauthorized')
    }

    await info.remove()

    res
      .status(200)
      .json({ info })
  }))

  return router
}
