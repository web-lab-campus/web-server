const Router = require('express').Router
const passport = require('passport')
const wrap = require('./_util').wrap
const isGuest = require('./_util').isGuest

module.exports = function ({ User, secret }) {
  const router = new Router()

  // ================
  // GET CURRENT USER
  // ================

  router.get('/whoami', (req, res) => res.json(req.user))

  // =====
  // LOGIN
  // =====

  router.post('/login', isGuest, (req, res, next) => {
    passport.authenticate('local', (err, user, info) => {
      if (err) return next(err)

      if (!user) {
        return res
          .status(422)
          .json(info)
      }

      req.login(user, err => {
        if (err) return next(err)

        if (req.body.remember) {
          req.session.cookie.maxAge = 30 * 24 * 3600 * 1000
        } else {
          req.session.cookie.expires = false
        }

        res.json({ user })
      })
    })(req, res, next)
  })

  // ========
  // REGISTER
  // ========

  router.post('/register', isGuest, wrap(async (req, res, next) => {
    const user = new User(req.body)
    await user.save()

    req.login(user, err => {
      if (err) return next(err)
      res.status(200).json({ user })
    })
  }))

  // ======
  // LOGOUT
  // ======

  router.get('/logout', (req, res) => {
    req.logout('/')
    res.json({ success: true })
  })

  return router
}
