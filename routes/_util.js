function createValidationError (err, res) {
  const errors = {}

  Object
    .values(err.errors)
    .forEach(r => {
      errors[r.path] = errors[r.path]
        ? `${errors[r.path]}, ${r.message}`
        : r.message
    })

  return res
    .status(422)
    .json({ errors })
}

const wrap = fn => (req, res, next) => fn(req, res, next).catch(err => {
  if (err.name === 'ValidationError') return createValidationError(err, res)
  next(err)
})

const isGuest = (req, res, next) => req.user
  ? res.redirect('/')
  : next()

const isLogin = (req, res, next) => req.user
  ? next()
  : res.redirect('/')

module.exports = {
  wrap,
  isGuest,
  isLogin
}
