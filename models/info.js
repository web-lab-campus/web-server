const mongoose = require('mongoose')
const Schema = mongoose.Schema

const InfoSchema = new Schema({
  title: {
    type: String,
    required: [true, 'Judul tidak boleh kosong'],
    minlength: [10, 'Judul tidak boleh kurang dari 10 karakter'],
    maxlength: [140, 'Judul tidak boleh lebih dari 140 karakter']
  },
  content: {
    type: String,
    required: [true, 'Isi tidak boleh kosong'],
    minlength: [50, 'Isi tidak boleh kurang dari 50 karakter']
  },
  author: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  }
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  }
})

InfoSchema.index({ title: 'text', content: 'text' })

module.exports = {
  Model: mongoose.model('Info', InfoSchema, 'informations'),
  Schema: InfoSchema
}
