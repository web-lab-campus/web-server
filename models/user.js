const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const Schema = mongoose.Schema

const UserSchema = new Schema({
  name: {
    type: String,
    required: [true, 'Nama tidak boleh kosong'],
    minlength: [4, 'Nama tidak boleh kurang dari 4 karakter'],
    maxlength: [40, 'Nama tidak boleh lebih dari 40 karakter'],
    match: [/^[a-zA-Z0-9\s.]+$/, 'Format nama hanya boleh terdiri dari huruf, angka, spasi dan titik']
  },
  username: {
    type: String,
    unique: 'Username sudah dipakai orang lain',
    required: [true, 'Username tidak boleh kosong'],
    minlength: [4, 'Username tidak boleh kurang dari 4 karakter'],
    maxlength: [24, 'Username tidak boleh lebih dari 24 karakter'],
    match: [/^[a-zA-Z0-9_]+$/, 'Format username hanya boleh terdiri dari huruf, angka dan garis bawah']
  },
  password: {
    type: String,
    select: false,
    minlength: [8, 'Password sedikitnya 8 karakter']
  },
  roles: [String]
})

UserSchema.pre('save', async function (next) {
  if (this.isModified('password')) {
    this.password = await bcrypt.hash(this.password, 10)
  }

  next()
})

UserSchema.methods.checkPassword = async function (password) {
  const matched = await bcrypt.compare(password, this.password)
  return matched
}

UserSchema.plugin(require('mongoose-beautiful-unique-validation'))

module.exports = {
  Model: mongoose.model('User', UserSchema, 'users'),
  Schema: UserSchema
}
