const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ConfigSchema = new Schema({
  key: String,
  value: Schema.Types.Mixed
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  }
})

module.exports = {
  Model: mongoose.model('Config', ConfigSchema, 'configs'),
  Schema: ConfigSchema
}
